package com.stock.mvc.dao;

import java.util.List;

import com.stock.mvc.entity.Salaire;

public interface ISalaireDao {

	Salaire save(Salaire entity);

	Salaire update(Salaire entity);

	List<Salaire> selectAll();

	List<Salaire> selectAll(String sortField, String sort);

	Salaire getById(Long id);

	void remove(Long id);

	Salaire findOne(String[] paramNames, Object[] paramValues);

	Salaire findOne(String paramName, String paramValue);

	int findCountBy(String paramName, String paramValue);

}
