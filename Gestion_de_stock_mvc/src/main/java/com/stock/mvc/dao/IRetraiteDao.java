package com.stock.mvc.dao;

import java.util.List;

import com.stock.mvc.entity.Retraite;

public interface IRetraiteDao {

	Retraite save(Retraite entity);

	Retraite update(Retraite entity);

	List<Retraite> selectAll();

	List<Retraite> selectAll(String sortField, String sort);

	Retraite getById(Long id);

	void remove(Long id);

	Retraite findOne(String[] paramNames, Object[] paramValues);

	Retraite findOne(String paramName, String paramValue);

	int findCountBy(String paramName, String paramValue);

}
