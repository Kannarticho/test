package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IRemBruteDao;
import com.stock.mvc.entity.RemBrute;
import com.stock.mvc.services.IRemBruteService;

@Transactional
public class RemBruteServiceImpl implements IRemBruteService{
	private IRemBruteDao dao;

	public void setDao(IRemBruteDao dao) {
		this.dao = dao;
	}

	@Override
	public RemBrute save(RemBrute entity) {
		return dao.save(entity);
	}

	@Override
	public RemBrute update(RemBrute entity) {
		return dao.update(entity);
	}

	@Override
	public List<RemBrute> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<RemBrute> selectAll(String sortField, String sort) {
		return  dao.selectAll(sortField, sort);
	}

	@Override
	public RemBrute getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public RemBrute findOne(String paramNames, Object[] paramValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RemBrute findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public RemBrute findOne(String paramName, String paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	
	
}
