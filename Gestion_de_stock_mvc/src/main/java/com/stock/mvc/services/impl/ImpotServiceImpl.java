package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IImpotDao;
import com.stock.mvc.entity.Impot;
import com.stock.mvc.services.IImpotService;

@Transactional
public class ImpotServiceImpl implements IImpotService{
	private IImpotDao dao;

	public void setDao(IImpotDao dao) {
		this.dao = dao;
	}

	@Override
	public Impot save(Impot entity) {
		return dao.save(entity);
	}

	@Override
	public Impot update(Impot entity) {
		return dao.update(entity);
	}

	@Override
	public List<Impot> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Impot> selectAll(String sortField, String sort) {
		return  dao.selectAll(sortField, sort);
	}

	@Override
	public Impot getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Impot findOne(String paramNames, Object[] paramValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Impot findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public Impot findOne(String paramName, String paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	
	
}
