package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entity.Impot;

public interface IImpotService {
	public Impot save(Impot entity);
	public Impot update(Impot entity);
	public List<Impot> selectAll();
	public List<Impot> selectAll(String sortField, String sort);
	public Impot getById(Long id);
	
	public void remove(Long id);
	public Impot findOne(String paramNames, Object[] paramValue);
	public Impot findOne(String[] paramNames, Object[] paramValues);
	public Impot findOne(String paramName, String paramValue);
	int findCountBy(String paramName, String paramValue);
}
