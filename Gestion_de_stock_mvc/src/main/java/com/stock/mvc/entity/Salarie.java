package com.stock.mvc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SALARIE")
public class Salarie implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idSal;
	@Column(name="NOM")
	private String nom;
	@Column(name="PRENOM")
	private String prenom;
	@Column(name="NUMSECU")
	private int numSecu;
	
	@ManyToOne
	@JoinColumn(name="idEmployeur")
	private Employeur emp;

	public Salarie(long idSal, String nom, String prenom, int numSecu, Employeur emp) {
		super();
		this.idSal = idSal;
		this.nom = nom;
		this.prenom = prenom;
		this.numSecu = numSecu;
		this.emp = emp;
	}
	
	public Salarie() {

	}

	public Long getIdSal() {
		return idSal;
	}

	public void setIdSal(Long idSal) {
		this.idSal = idSal;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getNumSecu() {
		return numSecu;
	}

	public void setNumSecu(int numSecu) {
		this.numSecu = numSecu;
	}

	public Employeur getEmp() {
		return emp;
	}

	public void setEmp(Employeur emp) {
		this.emp = emp;
	}
}
